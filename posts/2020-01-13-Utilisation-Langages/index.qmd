---
title: "GdT-Pyr 11 - Python dans R et R dans Python"
author: "Arnaud Charleroy et Céline Casenave"
date: 2020-01-13T21:13:14-05:00
tags: ["python", "R","reticulate"]
categories: [code, R, Python]
toc: true
number-sections: true
---


<div style="background-color:rgba(0, 255, 0,0.1); text-align:left; vertical-align: center; padding:10px 0;">
Comment utiliser:

* Python dans R
* R dans Python
* C/C++ dans R ou Python

https://forgemia.inra.fr/gdtpyr/gdt_pyr/-/tree/main/GDT_PyR11_utilisation_langages
</div>

![](reticulated_python.png){width="100"}
