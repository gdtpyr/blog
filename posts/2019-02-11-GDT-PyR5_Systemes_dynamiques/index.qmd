---
title: "GdT-Pyr 5 - Systèmes dynamiques et Optimisation"
author: "C. Casenave & G. Gnanguenon"
date: "2019-02-11T21:13:14-05:00"
categories: [analysis, methods, Python]
tags: ["optimisation"]
toc: true
number-sections: true
---

Séance 5 du groupe de travail PyR sur les **systèmes dynamiques et l'optimisation**:

<div style="background-color:rgba(0, 255, 0,0.1); text-align:left; vertical-align: center; padding:10px 0;">
Code Python: https://forgemia.inra.fr/gdtpyr/gdt_pyr/-/tree/main/GDT_PyR5_Systemes_dynamiques_Optim_Python

Code R: https://forgemia.inra.fr/gdtpyr/gdt_pyr/-/tree/main/GDT_PyR5_Systemes_dynamiques_Optim_R
</div>
