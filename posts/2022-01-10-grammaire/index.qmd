---
title: "Elaboration de graphiques à base de grammaire"
author: "Arnaud Charleroy (MISTEA) - I.Sanchez (MISTEA) pour la mise en forme"
date: 2022-01-01T04:08:13-06:00
categories: [visualisation, code, graphics, R, Python]
toc: true
number-sections: true
tags: ["grammaire", "grammar", "graphics", "graphiques", "ggplot2"]
---

<div style="background-color:rgba(0, 255, 0,0.1); text-align:left; vertical-align: center; padding:10px 0;">
Session du 10/01/2022: Introduction à la grammaire de graphiques. Ce post est un extrait de la fiche disponible à l'adresse suivante:

https://forgemia.inra.fr/gdtpyr/gdt_pyr/-/tree/main/GDT_PyR_19_Grammaire_Visualisation

* Portabilité
* Interopérabilité
* Pérennité (Version)
* Reproductibilité
* Génération de graphique
</div>

Ce post a pour but de présenter des outils de visualisation liés à une grammaire pour permettre une réutilisation, un partage, une compréhension et une évolution des graphiques plus riche.

[Lien GdT_PYR2 sur des graphiques en python](https://forgemia.inra.fr/isabelle.sanchez/GdT-PyR/-/tree/master/GDT_PyR2_Graphs_Python) 

## Graphiques à base de grammaire

Prérequis :

* Installer **plotnine** (python) et/ou **ggplot2** (R)
* Installer **altair** (python et R)

### Définition

Étude objective et systématique des éléments (phonèmes, morphèmes, mots) et des procédés (de formation, de construction, d'expression) qui constituent et caractérisent le système d'une langue naturelle.
[grammaire - cnrtl](https://www.cnrtl.fr/definition/grammaire)

Associée aux graphiques, elle est un cadre qui donne un vocabulaire et une sémantique qui permet de construire et de décrire des visualisations ou des graphiques de manière structurée. 
Une visualisation impliquant des données multidimensionnelles a souvent plusieurs composants ou aspects, et tirer partie de cette grammaire de graphiques en couches nous aide à décrire et à comprendre chaque composant impliqué dans la visualisation - en termes de **données, d'esthétique, d'échelle, d'objets**, etc.

![Figure extraite du livre "Grammar of Graphics"](graphic_grammar_book.png){width=500}

![](components.png){width=500}

### Intérêt et motivation

* Permettre une interprétation plus simple du graphique
* Favoriser la réutilisation
* Développer des outils basés sur la grammaire qui ne sont pas dépendants du langage de programmation


## Grammaire de graphiques (ggplot/plotnine)

**ggplot** est le résultat d'un travail réalisé par Hadley Wickham qui suit une approche de construction en couches, basé sur les travaux de Leland Wilkinson. [Wilkinson, Leland. The Grammar of Graphics. Springer Science & Business Media, 2005.](https://link.springer.com/book/10.1007/0-387-28695-0)

![](grammar_of_graphics.png){width=500}

Les grands principes de cette grammaire sont:

* **Données** : Commencez toujours par les données, identifiez les dimensions que vous souhaitez visualiser.
* **Esthétique** : Confirmez les axes en fonction des dimensions des données, des positions des différents points de données dans le tracé. Vérifiez également si une forme d'encodage est nécessaire, y compris la taille, la forme, la couleur, etc., qui est utile pour tracer plusieurs dimensions de données.
* **Échelle** : devons-nous mettre à l'échelle les valeurs potentielles, utiliser une échelle spécifique pour représenter plusieurs valeurs ou une plage ?
* **Objets géométriques** : ils sont communément appelés « *geoms* ». Cela couvrirait la façon dont nous représenterions les points de données sur la visualisation. Devrait-il s'agir de points, de barres, de lignes et ainsi de suite ?
* **Statistique**s : avons-nous besoin d'afficher des mesures statistiques dans la visualisation, telles que des mesures de tendance centrale, de propagation, d'intervalles de confiance ?
* **Facettes** : devons-nous créer des sous-graphiques basées sur des dimensions de données spécifiques ?
* **Système de coordonnées** : sur quel type de système de coordonnées la visualisation doit-elle être basée ? Doit-il être cartésien ou polaire ?

Cette grammaire a été implémentée dans le package **R**, **ggplot2**. Il y aussi une implémentation en python nommée [plotnine](https://plotnine.readthedocs.io/en/stable/).

**Ressources pratiques:**

* Exemples de graphique : https://juba.github.io/tidyverse/08-ggplot2.html
* Cheat sheet : https://github.com/rstudio/cheatsheets/blob/main/data-visualization-2.1.pdf

Un exemple plotnine en python:

```{}
from plotnine import *
from plotnine.data import mtcars

(ggplot(mtcars, aes('wt', 'mpg', color='factor(gear)'))
 + geom_point()
 + stat_smooth(method='lm')
 + facet_wrap('~gear'))
```

![](g1_plotnine.PNG){width=500}

### Ressources web

* [webin-R #08 : ggplot2 et la grammaire des graphiques](https://www.youtube.com/watch?v=msnwENny_cg)
* [A Comprehensive Guide to the Grammar of Graphics for Effective Visualization of Multi-dimensional Data](https://towardsdatascience.com/a-comprehensive-guide-to-the-grammar-of-graphics-for-effective-visualization-of-multi-dimensional-1f92b4ed4149)
* [Wilkinson, Leland. The Grammar of Graphics. Springer Science & Business Media, 2005.](https://link.springer.com/book/10.1007/0-387-28695-0)
* [grammaire - cnrtl](https://www.cnrtl.fr/definition/grammaire)
* [Data Camp - Grammar of graphics introduction](https://www.youtube.com/watch?v=uiTc55clwuA)

## Grammaire de visualisation (Vega and Vega-Lite -> Altair)

### Projet Vega et Vega lite

#### Définitions

* **Vega**

L'objectif du projet **Vega** est de promouvoir un écosystème d'outils utilisables et interopérables , prenant en charge des cas d'utilisation allant de l'analyse exploratoire des données à une communication efficace via une conception de visualisation personnalisée. (Même style que D3 mais en moins expressif)

Vega fournit **un langage formel et un format de fichier informatique pour représenter** et raisonner sur les visualisations. En d'autres termes, Vega fournit un moyen plus pratique mais puissant pour écrire des programmes qui génèrent des visualisations, allant des outils de conception interactifs aux outils de recommandation de graphiques automatiques. 

Vega fournit un environnement d'exécution performant et peut servir de « langage d'assemblage » pour la visualisation, permettant aux autres outils de se concentrer sur les questions de conception plutôt que sur les détails d'implémentation de bas niveau.

* **Vega-Lite**

**Vega-Lite** est une grammaire de haut niveau de graphiques interactifs. Il fournit une syntaxe JSON concise et déclarative pour créer une gamme expressive de visualisations pour l'analyse et la présentation des données.

Les spécifications Vega-Lite décrivent les visualisations comme des mappages de codage des données aux propriétés des marques graphiques (par exemple, des points ou des barres).

Le compilateur Vega-Lite produit automatiquement des composants de visualisation, notamment des axes, des légendes et des échelles. Il détermine les propriétés par défaut de ces composants sur la base d'un ensemble de règles soigneusement conçues.
Cette approche permet aux [spécifications](https://vega.github.io/vega-lite/docs/aggregate.html) Vega-Lite d'être concises pour une création de visualisation rapide, tout en donnant à l'utilisateur le contrôle pour remplacer les valeurs par défaut et personnaliser diverses parties d'une visualisation. 


#### Outils

* Exemple : https://vega.github.io/vega-lite/
* Éditeur : https://vega.github.io/editor

**Exemples pratiques** :

* https://vega.github.io/vega-lite/examples/
* [Vega au secours de vos visualisations de données (C. Woodrow)](https://www.youtube.com/watch?v=kKesanCy0a4)

Les deux projets **Vega** et **Vega-Lite** ont été créés par l'université de Washington au UW Interactive Data Lab (https://idl.cs.washington.edu/about/).

https://vega.github.io/

### Projet Altair

Altair =>  Portage de vega-lite sous R (https://vegawidget.github.io/altair/) et Python (https://altair-viz.github.io/getting_started/starting.html)  


```{}
import pandas as pd
data = pd.DataFrame({'a': list('CCCDDDEEE'),
                     'b': [2, 7, 4, 1, 2, 6, 8, 4, 7]})

import altair as alt
chart = alt.Chart(data)

alt.Chart(data).mark_bar().encode(
    x='a',
    y='average(b)'
)

import altair as alt
from vega_datasets import data

chart = alt.Chart(data.cars.url).mark_point().encode(
    x='Horsepower:Q',
    y='Miles_per_Gallon:Q',
    color='Origin:N',
).configure_view(
    continuousHeight=300,
    continuousWidth=400,
)
print(chart.to_json(indent=2))
print("first")

chart

print("two from spec")
chart2 = alt.Chart.from_json('''{
  "$schema": "https://vega.github.io/schema/vega-lite/v5.json",
  "data": {"url": "https://raw.githubusercontent.com/vega/vega/master/docs/data/seattle-weather.csv"},
  "mark": "bar",
  "encoding": {
    "x": {"timeUnit": "month", "field": "date", "type": "ordinal"},
    "y": {"aggregate": "mean", "field": "precipitation"}
  }
}''')
chart2
```


Références :

* [Vega au secours de vos visualisations de données (C. Woodrow](https://www.youtube.com/watch?v=kKesanCy0a4)
* [Documentation Altair Python](https://altair-viz.github.io/getting_started/starting.html)
* [Documentation Altair R](https://vegawidget.github.io/altair/)

### Conclusion

* Portabilité
* Interopérabilité
* Pérennité (Version)
* Reproductibilité
* Génération de graphique
