# blog GdT_PyR

![](PyR_logo.PNG){width="80"}

This is a blog website for the GdT_PyR group of the MISTEA lab using the [**Quarto**](https://quarto.org/) technology.

The GdT-PyR provides some tips for **R** and **Python** languages, feel free to browse the blog. A gitlab repository is linked to this blog, describing in full details the posts in the 2 languages:

https://gdtpyr.pages.mia.inra.fr/blog/

New addition in 2023: **Julia**!

© 2020-2024 - GPL3 License - INRAE MISTEA

Creation: [Isabelle Sanchez](https://forgemia.inra.fr/isabelle.sanchez) (INRAE MISTEA) with the useful help of [Arnaud Charleroy](https://forgemia.inra.fr/arnaud.charleroy) (INRAE MISTEA) and the GdT-PyR group.

![INRAE](INRAE_logo.png){width="100"}

![Mathnum](logo_mathnum.jpg){width="200"}
